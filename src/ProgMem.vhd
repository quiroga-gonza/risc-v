library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;


entity ProgMem is
    generic(
        ancho_inst:     integer := 8;
        ancho_address:  integer := 8
    );
    port(
        CLK_i:   in   std_logic;
        RST_i: in   std_logic;
        ADDR_i:  in   std_logic_vector(ancho_address-1 downto 0);
        DATA_o:  out  std_logic_vector((4*ancho_inst)-1 downto 0)
    );
end ProgMem;

architecture ProgMem_arch of ProgMem is
    
    type memory_array is array (2**ancho_address-1 downto 0) of std_logic_vector (ancho_inst-1 downto 0);
    
    impure function Ini_rom_file (file_name : string) return memory_array is
        file        rom_file:   text open read_mode is file_name;
        variable    rom_line:   line;
        variable    rom_value:  bit_vector(ancho_inst-1 downto 0);
        variable    temp:       memory_array;
    begin
        for rom_index in 0 to 2**ancho_address-1 loop
            readline(rom_file,rom_line);
            read(rom_line,rom_value);
            temp(rom_index) := to_stdlogicvector(rom_value);
        end loop;
        return temp;
    end function;

    constant my_memory: memory_array   := Ini_rom_file("../Program.txt");
    
    begin
    
    process(CLK_i,RST_i)
        begin
            if RST_i = '1' then
                DATA_o <= (others => '0');
            elsif(CLK_i'event and CLK_i = '1') then
                DATA_o <= my_memory((to_integer(unsigned(ADDR_i)+0))) & my_memory((to_integer(unsigned(ADDR_i)+1))) & my_memory((to_integer(unsigned(ADDR_i)+2))) & my_memory((to_integer(unsigned(ADDR_i)+3)));
            end if;
    end process;

end ProgMem_arch;
